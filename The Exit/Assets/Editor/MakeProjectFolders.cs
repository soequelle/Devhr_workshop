using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

//Clase que genera los folders de manera automática.
public class MakeProjectFolders : MonoBehaviour {

	// En la barra de menú agrega el Menu "Project Tools" y el submenu "Make Folders"
    [MenuItem ("Project Tools /Make Folders #_f")]
	
    static void MakeFolders () {
        GenerateFolders();
    }
	
	static void GenerateFolders(){
		
		string path = Application.dataPath + "/";  //guarda el path para las carpetas
		
		//Creación de las carpetas
		Directory.CreateDirectory(path + "Audio");
		Directory.CreateDirectory(path + "/Materials");
		Directory.CreateDirectory(path + "/3D/fbx");
		Directory.CreateDirectory(path + "/Fonts");
		Directory.CreateDirectory(path + "/Textures");
		Directory.CreateDirectory(path + "/Resources");
		Directory.CreateDirectory(path + "/Scripts");
		Directory.CreateDirectory(path + "/Shaders");
		Directory.CreateDirectory(path + "/Packages");
		Directory.CreateDirectory(path + "/Scenes");
		
		AssetDatabase.Refresh ();
	}
}
