﻿using UnityEngine;
using System.Collections;

public class CountDownTimer : MonoBehaviour {
	public GameObject HUDLost;
	private float countdown=0.0f;
	private bool timing=false;

	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
		if(timing){
    		countdown -= Time.deltaTime;
			this.guiText.text=countdown.ToString("F2");
    		if(countdown <= 0){
    			
				Instantiate(HUDLost);
    			timing = false;
    		}
    	}
	
	}
	
	public void StartTimer(float time){
		//print("enciende reloj");
    	timing = true;
    	countdown = time;
    }
	
	public void StopTimer(){
		
    	timing = false;
    	
    }
}
