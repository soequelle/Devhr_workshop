﻿using UnityEngine;
using System.Collections;

public class ObjectToRecolect : MonoBehaviour {
	private GamePlay gp;
	private  bool touched=false;
	
	
	void Awake(){
		gp=GameObject.Find("GamePlay").GetComponent<GamePlay>();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ObjectTouched(){
		if(!touched){
			touched=true;
			gp.RecolectItem();
			Destroy (gameObject);
		
		}
	}
}
