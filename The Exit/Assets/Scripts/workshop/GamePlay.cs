﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {
	public int items=0;
	public GameObject door;
	public int totalItems=0;
	public GameObject HUDWin;
	private HUDController Hud;
	// Use this for initialization
	void Start () {
		Hud=GameObject.Find("HUD").GetComponent<HUDController>();
		totalItems=GameObject.FindGameObjectsWithTag("ObjectToRecolect").Length;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void RecolectItem(){
		Hud.ItemActive(items);
		items++;
		CheckComplete();
	}
	
	public void Finish(){
		Hud.StopClock();
		Instantiate(HUDWin);
	}
	
	void CheckComplete(){
		if(totalItems==items){
			door.SetActive(false);
			Hud.InitClock();
		}
	}
	
	
}
